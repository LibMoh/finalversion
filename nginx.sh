#!/bin/bash
sudo -s
yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm

yum -y install nginx

sudo mkdir /home/web

sudo cp -f /vagrant/nginx.conf /etc/nginx/nginx.conf

echo "
<!DOCTYPE html>
<html>
<head>
Liban Mohamed CentOS NGINX
</head>
" >/home/web/index.html
systemctl enable nginx
systemctl start nginx
